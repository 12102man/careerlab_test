import axios from 'axios'


const coreApi = axios.create({
    baseURL: 'https://cors-anywhere.herokuapp.com/https://fakedata.dev/users/v1/',
    headers: {
        'origin': '*'
    }
});

export default coreApi
