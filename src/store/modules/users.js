import _ from "lodash"

export default {
    namespaced: true,
    state: {
        listOfUsers: [],
        selectedUser: undefined
    },
    mutations: {
        saveUsers(state, data) {
            state.listOfUsers = _.filter(data,user => parseInt(user.id) % 2 === 0);
        },
        openUserInfo(state, id) {
            state.selectedUser = _.find(state.listOfUsers, user => user.id === id);
        },
        closeUserInfo(state) {
            state.selectedUser = undefined
        }
    },
    actions: {
        getUsers({rootGetters, commit}) {
            rootGetters.api.get('get_users').then(resp => {
                commit('saveUsers', resp.data)
            })
        },
        selectUser({commit}, id) {
            commit('openUserInfo', id)
        },
        closeUser({commit}) {
            commit('closeUserInfo')
        }
    }
}
