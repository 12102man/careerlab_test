import Vue from 'vue'
import Vuex from 'vuex'
import users from './modules/users'
import coreApi from './api'
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    users
  },
  getters: {
    api: () => coreApi
  }
})
