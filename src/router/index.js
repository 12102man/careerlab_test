import Vue from 'vue'
import VueRouter from 'vue-router'
import Candidates from "@/components/Candidates";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/candidates'
  },
  {
    path: '/candidates',
    component: Candidates
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
